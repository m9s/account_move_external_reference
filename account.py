#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.modules.account.move import _MOVE_STATES, _MOVE_DEPENDS


class Move(ModelSQL, ModelView):
    _name = 'account.move'

    external_reference = fields.Char('External Reference', states=_MOVE_STATES,
        depends=_MOVE_DEPENDS)

Move()


class Line(ModelSQL, ModelView):
    _name = 'account.move.line'

    external_reference = fields.Char('External Reference')

Line()
